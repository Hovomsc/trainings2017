Function prototype declares a function, its return type, its parameters (it isn't necessary to declare parameters name in the prototype as it is done in the function definition). It only tells how much memory must be reserved, but it doesn't reveal inner construction of the function.

