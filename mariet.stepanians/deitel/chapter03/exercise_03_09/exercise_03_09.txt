A program could create string variables without a using declaration if each occurrence of the word string were prefixed by the namespace std, i.e. std::string.
